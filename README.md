# Quick Notes (qn)

Script para anotações rápidas pela linha de comandos.

## Princípios

- Filosofia UNIX: o script se limita a registrar as notas.
- Simplicidade: notas rápidas são textos curtos.

## Funcionalidades

- Insere texto digitado diretamente na linha de comandos.
- Recebe fluxos de texto pela entrada padrão.
- Anotações agrupadas por data em arquivos separados.
- Busca e navegação das notas utilizando apenas ferramentas GNU.
- Edição e remoção das notas no editor de textos selecionado.
- Se o primeiro caractere for '+', programa uma notificação.
